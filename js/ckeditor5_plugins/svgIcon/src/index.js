// cspell:ignore svgicon

import SvgIcon from './svgicon';

/**
 * @private
 */
export default {
  SvgIcon,
};
